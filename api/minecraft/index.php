<?php

$type = '0';
$DIR = "/home/runner/pseudo-api";

require_once "$DIR/src/classes.php";

if(isset($_GET["version"]) and isset($_GET["flavour"])) 
{   

    $json = new jsonHandler;
    $html = new htmlHandler;
    $dl = new downloadHandler;

    $json->init('https://launchermeta.mojang.com/mc/game/version_manifest.json');

    // Find jsonURL and downloadURL
    $jsonURL = $json->getURL($json->count());
    echo "$jsonURL";
    $html->none($jsonURL);
    $downloadURL = $dl->getDownload($jsonURL, $type);
    
    // Check downloadURL
    $html->none($downloadURL); 
    $html->check($downloadURL);

    // Redirect
    $html->redirect($downloadURL);

    unset($json, $html, $dl);
    $dl = null;
    $json = null;
    $html = null;

} else {

    $html = new htmlHandler;
    $html->httpNotFound;

    unset($html);
    $html = null;

}
?>