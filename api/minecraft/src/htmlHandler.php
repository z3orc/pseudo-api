<?php

class htmlHandler{

    public $httpError;
    public $httpNotFound;

    function __construct()
    {
        $this->httpError = http_response_code(500);
        $this->httpNotFound = http_response_code(404);
    }

    function none($downloadURL)
    {
        if(empty($downloadURL)){
            $this->httpError;
            exit;
        } elseif($downloadURL = null) {
            $this->httpError;
            exit;
        }
    }

    function check($downloadURL)
    {
        $headers = get_headers($downloadURL, 1);

        // if ($headers[0] == 'HTTP/1.1 200 OK' or $headers[0] == 'HTTP/1.0 200 OK') {
        //     echo "100";
        //     http_response_code(404);
        //     }

        if ($headers[0] == 'HTTP/1.1 404 Not Found' or $headers[0] == 'HTTP/1.0 404 Not Found') {
            $this->httpNotFound;
            exit();
        }
    }

    function redirect($downloadURL)
    {
        if (headers_sent() === false)
        {
            header("HTTP/1.1 303 See Other");
            header('Location: ' . $downloadURL);
        }

        exit();
    }

}

?>