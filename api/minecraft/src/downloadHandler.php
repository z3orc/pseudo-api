<?php

class downloadHandler
{

    private $downloadURL;
    private $version;
    private $flavour;

    function __construct()
    {
        $this->flavour = $_GET["flavour"];
        $this->version = $_GET["version"];
    }

    // function flavour()
    // {

    //     if(0 === strpos($this->flavourInput, 'va')){
    //         $this->flavour = "0";

    //     } elseif(0 === strpos($this->flavourInput, 'pa') or ($this->flavourInput === "paper"))  {
    //         $this->flavour = "1";

    //     } elseif(0 === strpos($this->flavourInput, 'pu') or ($this->flavourInput === "purpur")) {
    //         $this->flavour = "2";

    //     }

    //     return $this->flavour;
    // }

    function getDownload($jsonURL, $type){

        if($type === '0') {

            if(0 === strpos($this->flavour, 'va') or ($this->flavour === "vanilla")){
                $flavour = "0";
    
            } elseif(0 === strpos($this->flavour, 'pa') or ($this->flavour === "paper"))  {
                $flavour = "1";
    
            } elseif(0 === strpos($this->flavour, 'pu') or ($this->flavour === "purpur")) {
                $flavour = "2";
    
            }
    
            if($flavour === "0") {

                $json = file_get_contents($jsonURL);
    
                $data = json_decode($json);
    
                $downloadURL =  $data->downloads->server->url;
                $this->downloadURL = $downloadURL;
                return $downloadURL;
    
            } elseif ($flavour === "1") {
                $this->downloadURL = 'https://papermc.io/api/v1/paper/' . $this->version . '/latest/download';
                return $this->downloadURL;
    
            } elseif ($flavour === "2") {
                $this->downloadURL = 'https://purpur.pl3x.net/api/v1/purpur/' . $this->version . '/latest/download';
                return $this->downloadURL;
    
            }
        } elseif ($type === '1') {
            $this->downloadURL = 'https://minecraft.azureedge.net/bin-linux/bedrock-server-' . $this->version . '.zip';
            return $this->downloadURL;
        }     
    }

}

?>