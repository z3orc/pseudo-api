<?php

class jsonHandler{

    public $json;
    private $jsonURL;
    public $reqVersion;

    function __construct()
    {
        $this->reqVersion = $_GET["version"];
    }

    function init($json)
    {
        $this->json = file_get_contents($json);
    }

    function count()
    {
        $dataCount = json_decode($this->json, true);

        foreach ($dataCount as $key => $value) {
            $jsonLength = count($value);
        }

        return $jsonLength;
    }

    function getURL($jsonLength)
    {
        $data = json_decode($this->json);

        $version = "";
        $x = 0;

        while($x < $jsonLength and $version != $this->reqVersion) {
        $version = $data->versions[$x]->id;
        $url = $data->versions[$x]->url;
        
        $x++;
        
        }

        if($x === $jsonLength){
            $this->jsonURL = null;
        } else {
            $this->jsonURL = $url;
        }

        return $this->jsonURL;
    }

}

?>