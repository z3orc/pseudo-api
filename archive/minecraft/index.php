<?php

    function getPackageJson($reqVersion){

        $json = file_get_contents('https://launchermeta.mojang.com/mc/game/version_manifest.json');

        $dataCount = json_decode($json, true);

        foreach ($dataCount as $key => $value) {
            $y = count($value);
        }

        $data = json_decode($json);

        $version = "";
        $type = "";
        $downloadURL = "";

        $z = 0;
        $x = 0;

        while($x < $y and $version != $reqVersion) {
        $version = $data->versions[$x]->id;
        $type = $data->versions[$x]->type;
        $url = $data->versions[$x]->url;
        
        $x++;
        
        }

        // echo "$version - $type - $url";

        return $url;
    }

    function getDownloadURL($jsonURL){
        $json = file_get_contents($jsonURL);

        $data = json_decode($json);

        $downloadURL =  $data->downloads->server->url;
        return $downloadURL;

    }

    function testDownloadURL($downloadURL){
        if(empty($downloadURL)){
            http_response_code(404);
            exit;
        }

        $headers = get_headers($downloadURL, 1);

        // if ($headers[0] == 'HTTP/1.1 200 OK' or $headers[0] == 'HTTP/1.0 200 OK') {
        //     echo "100";
        //     http_response_code(404);
        //     }

        if ($headers[0] == 'HTTP/1.1 404 Not Found' or $headers[0] == 'HTTP/1.0 404 Not Found') {
            http_response_code(404);
            exit();
        }

    }

    function redirect($url, $permanent = false){
        
        if (headers_sent() === false)
        {
            header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
        }

        exit();
    }

    if(isset($_GET["version"]) and isset($_GET["flavour"])) {
        $flavour = $_GET["flavour"];
        $version = $_GET["version"];

        if(0 === strpos($flavour, 'va')){
            $reqVersion = $version;

            $downloadURL = getDownloadURL($jsonURL = getPackageJson($reqVersion));
            testDownloadURL($downloadURL);

            redirect($downloadURL, false);
        } elseif(0 === strpos($flavour, 'sp') or ($flavour === "spigot")) {

        } elseif(0 === strpos($flavour, 'pa') or ($flavour === "paper"))  {
            $downloadURL = 'https://papermc.io/api/v1/paper/' . $version . '/latest/download';

            testDownloadURL($downloadURL);

            redirect($downloadURL, false);

            // $headers = get_headers($downloadURL, 1);
            // var_dump($headers);
        } elseif(0 === strpos($flavour, 'pu') or ($flavour === "purpur")) {
            $downloadURL = 'https://purpur.pl3x.net/api/v1/purpur/' . $version . '/latest/download';

            testDownloadURL($downloadURL);

            redirect($downloadURL, false); 

            // $headers = get_headers($downloadURL, 1);
            // var_dump($headers);
        } else {
            header("HTTP/1.0 404");
            exit;
        }

    }

?>